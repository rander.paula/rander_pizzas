package br.com.example.helloworld;

import android.os.Bundle;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate (@Nullable Bundle saveInstanceState)
    {
        super.onCreate(saveInstanceState);
        TextView txtHelloWorld = new TextView(this);
        txtHelloWorld.setText("Hello World");
        setContentView(txtHelloWorld);
    }
}
